import React, { useState } from 'react';
import './Create.css';

import Enveloppe from '../../media/circle-enveloppe.png';
import Lock from '../../media/circle-lock.png';
import Lockre from '../../media/circle-lock-re.png';
import Phone from '../../media/circle-phone.png';
import User from '../../media/circle-user.png';

export default function Create() {
    const [firstname, setFirstname]=useState("")
    const [lastname, setLastname]=useState("")
    const [email, setEmail]=useState("")
    const [password, setPassword]=useState("")
    const [passwordre,setPasswordre]=useState("")
    const [phone, setPhone]=useState("")
    const [remember, setRemember]=useState(true)
    const [news, setNews]=useState(true)

  
    async function ajout(){
        let item={firstname,lastname,email,password,passwordre,phone,remember,news}
        console.warn(item)
         let result = await fetch(`/api/register`,{
             method:'POST',
             body:JSON.stringify(item),
             headers:{
                 "Content-Type":'application/json',
                 "Accept":'application/json'
             }
         })
         result = await result.json()
         console.warn("result", result)
    }
    return (
        <div className="creation-compte">
            <div className="head-banner">
                <img src={ process.env.PUBLIC_URL + '/header-banner.jpg' }/>
            </div>
            <div className="modal-form">
                <div className="modal-content">
                    <h1 className="form-title">Create New Account</h1>
                    <form action="">
                        <div className="form-line">
                            <div className="label-input half">
                                <div className="label">
                                    <label for="first-name">
                                        <img src={ User } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="text" name="firstname" value={firstname} onChange={(e)=>setFirstname(e.target.value)} placeholder="First Name"/>
                            </div>
                            <div className="spacer"></div>
                            <div className="input half">
                                <input type="text" name="lastname" value={lastname} onChange={(e)=>setLastname(e.target.value)} placeholder="Last Name"/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label for="email">
                                        <img src={ Enveloppe } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="email" name="email" value={email} onChange={(e)=>setEmail(e.target.value)} placeholder="Email"/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label for="password">
                                        <img src={ Lock } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} placeholder="Password"/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label for="password-re">
                                        <img src={ Lockre } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="password" name="passwordre" value={passwordre} onChange={(e)=>setPasswordre(e.target.value)} placeholder="Confirm Password"/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label for="phone">
                                        <img src={ Phone } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="text" name="phone" value={phone} onChange={(e)=>setPhone(e.target.value)} placeholder="Phone Number"/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="half">
                                <input type="checkbox" value={remember} onChange={(e)=>setRemember(e.target.value)} name="remember"/>
                                <label for="remember">Remember me</label>
                            </div>
                            <div className="half">
                                <input type="checkbox" value={news} onChange={(e)=>setNews(e.target.value)} name="news"/>
                                <label for="news">Subscribe to Newsletter</label>
                            </div>
                        </div>

                        <button onClick={ajout} className="form-button" type="submit">Register</button>
                    </form>
                </div>
                <img src={ process.env.PUBLIC_URL + '/geometry.jpg' } alt="geom" className='bottom-im'/>
            </div>
            
        </div>
    )
}
