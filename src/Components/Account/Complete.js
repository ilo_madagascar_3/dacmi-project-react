import React, { useRef } from 'react';
import './Complete.css';

import Calendar from '../../media/circle-calendar.png';
import Nation from '../../media/circle-nation.png';
import Locate from '../../media/circle-locate.png';
import House from '../../media/circle-house.png';
import World from '../../media/circle-world.png';
import Upload from '../../media/upload.png';


export default function Complete() {
    let uploadLogo= useRef(null);
    function triggerUpload(){
        uploadLogo.current.click();
    }
    return (
	<div class="completion-compte">
        <div class="head-banner">
                <img src={ process.env.PUBLIC_URL + '/header-banner.jpg' }/>
        </div>
        <div class="modal-form">
            <div class="modal-content">
                <h1 class="form-title">Complete your Account</h1>
                <form action="">
                    <div class="form-grid">
                        <div class="label">
                            <img src={ Calendar } alt="" width={'20%'}/>
                            <label htmlFor="naissance">
                                Naissance
                            </label>
                        </div>
                        <div class="input" name="naissance">
                            <input type="number" name="day" placeholder="dd"/>
                            <div class="spacer"></div>
                            <input type="number" name="month" placeholder="mm"/>
                            <div class="spacer"></div>
                            <input type="number" name="year" placeholder="yyyy"/>
                        </div>
                    </div>

                    <div class="form-grid">
                        <div class="label">
                            <img src={ Nation } alt="" width={'20%'}/>
                            <label htmlFor="nationalite">
                                Nationalité
                            </label>
                        </div>
                        <div class="input">
                            <select name="nationalite">
                                <option disabled selected value></option>
                                <option value="nt-1">Nationalité 1</option>
                                <option value="nt-2">Nationalité 2</option>
                                <option value="nt-3">Nationalité 3</option>
                                <option value="nt-4">Nationalité 4</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-grid">
                        <div class="label">
                            <img src={ Locate } alt="" width={'20%'}/>
                            <label htmlFor="address">
                                Address
                            </label>
                        </div>
                        <div class="input">
                            <input type="text" name="address"/>
                        </div>
                    </div>

                    <div class="form-grid">
                        <div class="label">
                            <img src={ House } alt="" width={'20%'}/>
                            <label htmlFor="city">
                                City
                            </label>
                        </div>
                        <div class="input" name="city">
                            <input type="text" name="city1"/>
                            <div class="spacer"></div>
                            <input type="text" name="city2"/>
                        </div>
                    </div>

                    <div class="form-grid">
                        <div class="label">
                            <img src={ World } alt="" width={'20%'}/>
                            <label htmlFor="country">
                                Country
                            </label>
                        </div>
                        <div class="input">
                            <select name="country">
                                <option disabled selected value></option>
                                <option value="nt-1">Country 1</option>
                                <option value="nt-2">Country 2</option>
                                <option value="nt-3">Country 3</option>
                                <option value="nt-4">Country 4</option>
                            </select>
                        </div>
                    </div>

                    <div class="label-input uploader">
                        <div class="label">
                            <label onClick= {triggerUpload} >
                            <img src={ Upload } alt="" width={'50%'}/>
                            </label>
                        </div>
                        <div className="input">
                            <label htmlFor="file-upload" className='label-upload' ref={ uploadLogo }>Upload your DOC,<br/> PDF or JPEG</label>
                            <input type="file" name="file" id='file-upload'/>
                        </div>
                    </div>

                    <button class="form-button" type="submit">Register</button>
                    <div class="denied invisible">
                        Nous n'acceptons pas les clients de votre région.<br/>
                        Veuillez contacter le service client
                    </div>
                </form>
            </div>
                <img src={ process.env.PUBLIC_URL + '/geometry.jpg' } alt="geom" className='bottom-im'/>
        </div>        
    </div>
    )
}


{/* let country= document.querySelector('select[name="country"]');
let denied= document.querySelector('.denied');

country.addEventListener('change', func);
function func(){
    denied.classList.toggle('invisible');
} */}