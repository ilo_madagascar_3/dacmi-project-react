import React, { useRef, useState, useEffect } from 'react';
import './Header.css';
import { ReactComponent as User} from '../../media/user.svg';


export default function Header() {
	const [menuShown, setMenuShown] = useState(false);//is the menu shown??

	let borderer= useRef(null);
	let hiddenMenu= useRef(null);

	function toggleMenu(){
		setMenuShown(!menuShown);
		borderer.current.classList.toggle('invisible');
		hiddenMenu.current.classList.toggle('invisible');
	}

	// const detect= function(event){
	// 	if(document.querySelector('.vertical-dots').contains(event.target)){
	// 		return;
	// 	}
	// 	else{
	// 		if(menuShown){
	// 			setMenuShown(false);
	// 			borderer.current.classList.add('invisible');
	// 			hiddenMenu.current.classList.add('invisible');
	// 		}
	// 	}
	// }

	// document.addEventListener('click', detect);
	

    return (
        <div className="header">
			<img className="header-banner" src={ process.env.PUBLIC_URL + '/myaccount.jpg' }/>
			<div className="header-bar">
				<div className="left">
					<div className="date">
						Jan 2022
					</div>
					<div className="crumb">
						Dashboard
					</div>
				</div>

				<div className="right">
					<div className="user-name">
						John Smith
					</div>

					<div className="avatar">
						<User width={ '70%' } height={ '70%' }></User>
					</div>

					<div className="vertical-dots" onClick={ toggleMenu }>
						<div className="dot"></div>
						<div className="dot"></div>
						<div className="dot"></div>
						<div className="borderer invisible" ref={ borderer }></div>
						<div className="hidden-menu invisible" ref={ hiddenMenu }>
							<ul className="menu-list">
								<li className="menu-item">Menu 1</li>
								<li className="menu-item">Menu 2</li>
								<li className="menu-item">Menu 3</li>
							</ul>
						</div>
					</div>

				</div>

			</div>

        </div>
    );
}
