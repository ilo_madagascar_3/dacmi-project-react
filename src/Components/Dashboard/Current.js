import React from 'react';
import './Current.css';
import { Line } from 'react-chartjs-2';

const data = {
  labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
  datasets: [
    {
      label: 'raise in %',
      data: [12, 79, 43, 35, 2, 3, 99, 52],
      fill: false,
      backgroundColor: '#b59600',
      borderColor: '#b5960033',
    //   backgroundColor: 'rgb(255, 99, 132)',
    //   borderColor: 'rgba(255, 99, 132, 0.2)',
    },
  ],
};

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};


export default function Current() {
    return (
        <div className="current-content">
            <Line data={data} options={options} />
            
            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
        </div>
    )
}
