import React from 'react';
import './Values.css';

export default function Values() {
    return (
        <div className="add-data">
            <div className="data" id="number-of-dacmi">
                <h2 className='number'>23400</h2>
                <p eplanation>Number of Dacmi buy</p>
                <div className="horizontal-dots">
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="menu-thing invisible">
                        <ul className="mini-list">
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="data" id="value-of-dacmi">
                <h2 className='number'><span id="dacmi-value">1.4</span> $</h2>
                <p eplanation>Dacmi value</p>
                <div className="horizontal-dots">
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="menu-thing invisible">
                        <ul className="mini-list">
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="data" id="meta-mask">
                <h2 className='meta'>Meta Mask</h2>
                <img src={ process.env.PUBLIC_URL + "metamask.png"}/>
                <div className="horizontal-dots">
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="menu-thing invisible">
                        <ul className="mini-list">
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
