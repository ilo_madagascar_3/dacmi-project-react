import React from 'react'

export default function Buy() {
    return (
        <div className="tabulable-content">
            <div className="my-key">
                <p className="key">
                    My adress key :
                    <span>
                        ox2bdfrg2rg5rg2163z32gr65z2gfe3r5g213s5gf3rg135erg435rg3er5g3er5
                    </span>
                </p>
            </div>
            <div className="methods">
                <div className="images">
                    <img src={ process.env.PUBLIC_URL + "coin-2.png" }/>
                    <img src={ process.env.PUBLIC_URL + "coin-3.png" }/>
                </div>
                <div className="cards">
                    <div className="payment-line">
                        <p>Payment by Card</p>
                        <img src={ process.env.PUBLIC_URL + "card.png" }/>
                    </div>
                    <div className="payment-lines">
                        <p>Payment by bank transfer</p>
                        <img src={ process.env.PUBLIC_URL + "rib.png" }/>
                    </div>
                </div>
            </div>
            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
        </div>
    )
}
