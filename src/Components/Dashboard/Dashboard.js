import React, { useState } from 'react';
import './Dashboard.css';
import Header from '../Header/Header';
import Current from './Current';
import Wallet from './Wallet';
import Buy from './Buy';
import Values from './Values';
import Stats from './Stats';

import pie from '../../media/pie-white.png';
import piecol from '../../media/pie-col.png';
import user from '../../media/username-white.png';
import usercol from '../../media/username-col.png';
import compass from '../../media/compass-white.png';
import compasscol from '../../media/compass-col.png';
import calendar from '../../media/calendar-white.png';
import calendarcol from '../../media/calendar-col.png';
import assist from '../../media/assist-white.png';
import assistcol from '../../media/assist-col.png';
import settings from '../../media/settings-white.png';
import settingscol from '../../media/settings-col.png';


export default function Dashboard() {
    
    const [activeTab, setActiveTab] = useState(0); //'buy' and 'wallet'
    function setTab(event){
        let data_index= event.target.getAttribute('data-tab');
        setActiveTab(parseInt(data_index));
    }
    
    return (
        <>
            <div className="my-account">
                <Header></Header>
                <div className="content">
                    <div className="side-menu">
                        <ul className="menu-list">
                            <li className="active">
                                <a href="dash" className="menu-link">
                                    <div className="superposed">
                                        <img src={ piecol } className='thing1'/>
                                        <img src={ pie } className='thing2'/>
                                    </div>
                                    <div className="text-link">
                                        Dashboard
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="accounts" className="menu-link">
                                    <div className="superposed">
                                        <img src={ usercol } className='thing1'/>
                                        <img src={ user }/>
                                    </div>
                                    <div className="text-link">
                                        Accounts
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="guide" className="menu-link">
                                    <div className="superposed">
                                        <img src={ compasscol } className='thing1'/>
                                        <img src={ compass }/>
                                    </div>
                                    <div className="text-link">
                                        Our guide
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="calendar" className="menu-link">
                                    <div className="superposed">
                                        <img src={ calendarcol } className='thing1'/>
                                        <img src={ calendar }/>
                                    </div>
                                    <div className="text-link">
                                        Calendar
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="assist" className="menu-link">
                                    <div className="superposed">
                                        <img src={ assistcol } className='thing1'/>
                                        <img src={ assist }/>
                                    </div>
                                    <div className="text-link">
                                        Assistance
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="settings" className="menu-link">
                                    <div className="superposed">
                                        <img src={ settingscol } className='thing1'/>
                                        <img src={ settings }/>
                                    </div>
                                    <div className="text-link">
                                        Settings
                                    </div>
                                </a>
                            </li>
                        </ul>

                        <div className="dacmi-logo">
                            <img src={ process.env.PUBLIC_URL + 'coin-1.png' }/>
                        </div>
                    </div>

                    {/* <Values></Values> */}
                    {/* <Stats></Stats> */}
                    { activeTab === 0 ? <Stats></Stats> : <Values></Values>}

                    <div className="tabulable">
                        <div className="tabs">
                            {/* <div className="tab">CURRENT</div> */}
                            <div className={ activeTab === 0 ? 'tab active' : 'tab'} onClick={setTab} data-tab='0'>CURRENT VALUE</div>
                            <div className={ activeTab === 1 ? 'tab active' : 'tab'} onClick={setTab} data-tab='1'>WALLET</div>
                            <div className={ activeTab === 2 ? 'tab active' : 'tab'} onClick={setTab} data-tab='2'>BUY DACMI</div>
                        </div>
                        { activeTab === 0 && <Current></Current> }
                        { activeTab === 1 && <Wallet></Wallet> }
                        { activeTab === 2 && <Buy></Buy> }
                        {/* <Wallet></Wallet> */}
                        {/* <Buy></Buy> */}
                    </div>
                </div>
            </div>
        </>
    )
}
