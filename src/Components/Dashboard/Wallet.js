import React from 'react';

export default function Wallet() {
    return (
        <div className="wallet-content">
            
             <div className="table">
                 <p className="table-title">
                    Statistique du transfert de jetons
                 </p>
                <table class="data">
                    <tr>
                        <th>Métrique</th>
                        <th>Valeur</th>
                    </tr>
                    <tr>
                        <td>Entry First Line 1</td>
                        <td>Entry First Line 2</td>
                    </tr>
                    <tr>
                        <td>Entry Line 1</td>
                        <td>Entry Line 2</td>
                    </tr>
                    <tr>
                        <td>Entry Last Line 1</td>
                        <td>Entry Last Line 2</td>
                    </tr>
                    <tr>
                        <td>Entry Last Line 1</td>
                        <td>Entry Last Line 2</td>
                    </tr>
                    <tr>
                        <td>Entry Last Line 1</td>
                        <td>Entry Last Line 2</td>
                    </tr>
                </table>
             </div>

            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
        </div>
    )
}
