import React from 'react'

export default function Stats() {
    return (
        <div className="add-data">
            <div className="data" id="number-of-views">
                <h2 className='number'><span className="arrow increase"></span>234</h2>
                <p eplanation>Dacmi value views</p>
                <div className="horizontal-dots">
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="menu-thing invisible">
                        <ul className="mini-list">
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="data" id="value-of-users">
                <h2 className='number'><span className="arrow decrease"></span><span id="dacmi-value">22</span> %</h2>
                <p eplanation>Dacmi value</p>
                <div className="horizontal-dots">
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="menu-thing invisible">
                        <ul className="mini-list">
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="data" id="new-users">
                <h2 className="number"><span className="arrow increase"></span>2.345</h2>
                <p explanation="">New users</p>
                <div className="horizontal-dots">
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="dot"></div>
                    <div className="menu-thing invisible">
                        <ul className="mini-list">
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                            <li className="mini-item">Menu 1</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
