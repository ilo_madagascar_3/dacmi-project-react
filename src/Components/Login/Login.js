import React, {useRef, useState} from 'react';
import './Login.css';
import { ReactComponent as User} from '../../media/user-c.svg';
import { ReactComponent as Lock} from '../../media/lock-c.svg';

import { Link } from 'react-router-dom';

export default function Login() {
    let linkToRegister= useRef(null);
    let linkToDashboard= useRef(null);

    function registerFunc(){
        linkToRegister.current.click();
    }

    const [email, setEmail]=useState("")
    const [password, setPassword]=useState("")

     async function Clogin(event){
       event.preventDefault();

        let item={email,password}
      
        let data  = await window.fetch(`/api/login`,{
            method:'POST',
            body:JSON.stringify(item),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            }, 
        })
        const resulta = await data.json();
        console.log(resulta);
        localStorage.setItem("userInfo", JSON.stringify(resulta));

        const userInfo = localStorage.getItem("userInfo");
        if(userInfo){
          linkToDashboard.current.click();
        }
    }

    return (
        <div className="page-connection container-single">
            <div className="logo-dacmi">
                <img src={ process.env.PUBLIC_URL + '/dacmi-center.png' }/>
            </div>
            <div className="connection-form-container">
                <form action="">
                    <div className="label-input" id="username">
                        <div>
                            <label htmlFor='userName'><User width={ '80%' } height={ '80%' }></User></label>
                        </div>
                        <input type="text" name="email" value={email} onChange={(e)=>setEmail(e.target.value)} placeholder="Username"/>
                    </div>
                    <div className="label-input" id="pass">
                        <div>
                            <label htmlFor='password'><Lock width={ '90%' } height={ '90%' }></Lock></label>
                        </div>
                        <input type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)}/>
                    </div>
                    <button type='submit' onClick={Clogin} className='form-button'>Login</button>
                    <button className="form-button" onClick={ registerFunc }>Get registered</button>
                    <Link className='invisible' to='/create-account' ref={ linkToRegister }></Link>
                    <Link className='invisible' to='/dashboard' ref={ linkToDashboard }></Link>
                </form>
            </div>
        </div>
    )
}
