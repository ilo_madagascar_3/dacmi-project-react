import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Login from './Components/Login/Login';
import Create from './Components/Account/Create';
import Complete from './Components/Account/Complete';
import Dashboard from './Components/Dashboard/Dashboard';

function App() {
  return (
    <>
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>

        <Route path='/' exact>
          <h1>Temporary home page</h1>
          <Link to='/login'>Login</Link>
          <Link to='/create-account'>Create Account</Link>
          <Link to='/complete-account'>Complete Account</Link>
          <Link to='/dashboard'>Dashboard</Link>
        </Route>

        <Route path='/login' exact component={ Login }>
        </Route>

        <Route path='/dashboard' exact component={ Dashboard }>
        </Route>

        <Route path='/create-account' exact component={ Create }>
        </Route>

        <Route path='/complete-account'  exact component={ Complete }>
        </Route>

      </Switch>
    </Router>
    </>
  );
}

export default App;
